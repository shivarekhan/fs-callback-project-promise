// Problem 1:

// Using callbacks and the fs module's asynchronous functions, do the following:
//     1. Create a directory of random JSON files
//     2. Delete those files simultaneously 

const fs = require('fs');
const path = require('path');


function createDirectory(dirPath) {
    const JsonFolderPath = path.join(__dirname, dirPath);
    return new Promise(function (resolve, reject) {
        fs.mkdir(JsonFolderPath, { recursive: true }, function (error) {
            if (error) {
                reject("ERROR : Directory couldn't be created", error);
            } else {
                console.log("SUCCESS : directory for random json files created .");
                resolve(JsonFolderPath);
            }
        });
    });
};

function createFile(jsonPath, fileData) {
    return new Promise(function (resolve, reject) {
        fs.writeFile(jsonPath, fileData, function (error) {
            if (error) {
                reject(error);
            } else {
                console.log(`SUCCESS : file ${jsonPath} created`);
                resolve(jsonPath);
            }
        });
    });
}

function deleteFile(jsonPath) {
    return new Promise(function (resolve, reject) {
        fs.unlink(jsonPath, function (error) {
            if (error) {
                reject(error);
            } else {
                console.log(`File ${jsonPath} deleted successfully`);
            }
        });
    });
}

function createRandomfiles(JsonFolderPath) {
    const randomNumber = Math.floor((Math.random() + 1) * 10);
    const filesArray = new Array(randomNumber).fill(1);
    const fileToCreate = filesArray.map(function (current) {

        const fileData = `random data ${(Math.random() + 1) * 100}`;
        const fileName = `randomJSON${(Math.random() + 1) * 10}.json`;
        const jsonPath = path.join(JsonFolderPath, "/" + fileName);

        return createFile(jsonPath, fileData);
    });

    return new Promise(function (resolve, reject) {
        Promise.all(fileToCreate)
            .then(function (createdFile) {
                resolve(createdFile)
            })
            .catch(function (error) {
                reject(error);
            });
    });
}


function deleteFiles(filesPath) {
    const filesToDelete = filesPath.map(function (currentFilePath) {
        return deleteFile(currentFilePath);
    });
    return new Promise(function (resolve, reject) {
        Promise.all(filesToDelete)
            .then(function (createdFile) {
                resolve(createdFile)
            })
            .catch(function (error) {
                reject(error);
            });
    });
}


function createAndDeleteRandomFiles(dirName) {
    createDirectory(dirName)
        .then(function (JsonFolderPath) {
            console.log(`FILE CREATION STARTED :`);
            return createRandomfiles(JsonFolderPath);
        })
        .then(function (filesPath) {
            console.log("FILE DELETION STARTED: ");
            return deleteFiles(filesPath);
        })
        .catch(function (error) {
            console.log(error);
        });
}



module.exports = createAndDeleteRandomFiles;