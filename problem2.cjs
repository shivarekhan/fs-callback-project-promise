// roblem 2:

//     Using callbacks and the fs module's asynchronous functions, do the following:
//         1. Read the given file lipsum.txt
//         2. Convert the content to uppercase & write to a new file. Store the name of the new file 
//            in filenames.txt
//         3. Read the new file and convert it to lower case. Then split the contents into sentences. 
//            Then write it to a new file. Store the name of the new file in filenames.txt
//         4. Read the new files, sort the content, write it out to a new file. Store the name of 
//             the new file in filenames.txt
//         5. Read the contents of filenames.txt and delete all the new files that are mentioned 
//            in that list simultaneously.

const path = require('path');
const fs = require('fs');

const lipsumPath = path.join(__dirname, "./lipsum.txt");
const upperCaseLipsumPath = path.join(__dirname, "./upperCaseLipsum.txt");
const filesNamesPath = path.join(__dirname, "./filenames.txt");
const lowerCaseSentencePath = path.join(__dirname, "./lowerCaseSentence.txt");
const sortedSentencePath = path.join(__dirname, "./sortedSentence.txt");


function readFile(filePath) {
    return new Promise(function (resolve, reject) {
        fs.readFile(filePath, 'utf-8', function (error, fileData) {
            if (error) {
                reject(error);
            }
            else {
                resolve(fileData);
            }
        });
    });
}

function createFile(filePath, fileData) {
    return new Promise(function (resolve, reject) {
        fs.writeFile(filePath, fileData, function (error) {
            if (error) {
                reject(error);
            } else {
                resolve(filePath);
            }
        });
    });
}

function writeFileName(filePath, fileData) {
    return new Promise(function (resolve, reject) {
        fs.appendFile(filePath, fileData + "\n", function (error) {
            if (error) {
                reject(error);
            } else {
                resolve(fileData);
            }
        })
    })
}

function deleteFile(filePath) {
    return new Promise(function (resolve, reject) {
        fs.unlink(filePath, function (error) {
            if (error) {
                reject(error);
            } else {
                resolve(filePath);
            }
        })
    })
}



function problem2() {
    console.log(`SUCCESS : Started reading ${lipsumPath}`);
    readFile(lipsumPath)
        .then(function (fileData) {
            console.log(`SUCCESS : Start converting in ${lipsumPath} data in UpperCase`);
            const upperCaseLipsum = fileData.toUpperCase();
            return createFile(upperCaseLipsumPath, upperCaseLipsum);
        })
        .then(function (filePath) {
            console.log(`SUCCESS : Start writing ${filePath} in filenames.txt`);
            return writeFileName(filesNamesPath, filePath);
        })
        .then(function () {
            console.log(`SUCCESS : Start reading file ${upperCaseLipsumPath}`);
            return readFile(upperCaseLipsumPath)
        })
        .then(function (fileData) {
            console.log(`SUCCESS : Start converting ${upperCaseLipsumPath} file data in lowercase and sentence`);
            const lowerCaseSentence = fileData.toLowerCase().split(".")
                .map(function (sentence) {
                    return sentence.trim();
                })
                .filter(function (sentence) {
                    return sentence !== "";
                }).join("\n");
            console.log(`SUCCESS : Writing the lowercase sentence data in ${lowerCaseSentencePath}`);
            return createFile(lowerCaseSentencePath, lowerCaseSentence);
        })
        .then(function (filePath) {
            console.log(`SUCCESS : writing ${filePath} in filesname.txr`);
            return writeFileName(filesNamesPath, filePath);
        })
        .then(function (filePath) {
            return readFile(filePath);
        })
        .then(function (filedata) {
            const sortedSentence = filedata.split("\n").sort().join("\n");
            return createFile(sortedSentencePath, sortedSentence);
        })
        .then(function (filePath) {
            return writeFileName(filesNamesPath, filePath);
        })
        .then(function () {
            return readFile(filesNamesPath);
        })
        .then(function (fileData) {
            const data = fileData.split("\n")
                .filter(function (currentFile) {
                    return currentFile!=="";
                })
                .map(function(currentFile){
                    return deleteFile(currentFile);
                });
            return Promise.all(data);

        })
        .catch(function(error){
            console.log(error);
        })

}
module.exports = problem2;